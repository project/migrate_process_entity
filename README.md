# Migrate Process Entity

This module provides an additional process plugin to get field values from Drupal 8 entities within a migration.

## Use

In your migration, you may want to retrieve a key field value from an entity which already exists. This could be one manually created, or one created via an upstream migration.

In your migration, use like any other process plugin:

```yaml
field_title:
  source: 'migrated_entity_id'
  plugin: entity_value
  entity_type: my_custom_entity
  field: field_name
```

Typically, this plugin is combined with an `entity_lookup` to get the target entity ID from an upstream migration:

```yaml
field_title:
  -
    plugin: migration_lookup
    migration: the_upstream_migration
    entity_type: my_custom_entity
    no_stub: true
  -
    plugin: entity_value
    entity_type: my_custom_entity
    field: field_name
```

Often, you may need the upstream entity ID in addition to its value. In that cause, you can use a 'psudo-pipeline' to only lookup the entity once:

```yaml
_migrated_entity_id:
    plugin: migration_lookup
    migration: the_upstream_migration
    entity_type: my_custom_entity
    no_stub: true
field_entity_ref: '@_migrated_entity_id'
field_title:
    source: '@_migrated_entity_id'
    plugin: entity_value
    entity_type: my_custom_entity
    field: field_name
```

You may also use constants and the `concat` module with multiple psudo-pipelines to create more complex values.

## Comparison with entity_exists and entity_lookup

Both core's `entity_exists` and Migrate Plus's `entity_lookup` can look up an existing Drupal entity and return its ID. `entity_value` can also do this, but that's not it's primary purpose.

The primary purpose is to retrieve field values from the entity instead of just the ID.

## Best practices

The `entity_value` plugin duplicates data, and as such, is really only intended for niche use. If possible, you should always rely on an entity reference field instead.

One use case is in a complex content model, an entity may require a title for the sake of the admin UI. That title may need to be generated based on which entities it references. In that case, this module is appropriate.
