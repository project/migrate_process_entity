<?php


namespace Drupal\migrate_process_entity\Plugin\migrate\process;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This plugin gets a field value on on entity if the given entity exists.
 *
 * Example usage with configuration:
 * @code
 *   field_tags:
 *     plugin: entity_value
 *     source: nid
 *     entity_type: node
 *     field: field_my_custom_field
 * @endcode
 *
 * @MigrateProcessPlugin(
 *  id = "entity_value"
 * )
 */
class EntityValue extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * EntityExists constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param $storage
   *   The entity storage.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->storage = $storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static($configuration, $plugin_id, $plugin_definition, $container
      ->get('entity_type.manager')
      ->getStorage($configuration['entity_type']));
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    // Get only the first value if an array is provided.
    if (is_array($value)) {
      $value = reset($value);
    }

    // Load the entity.
    $entity = $this->storage->load($value);

    // Work with the entity if found.
    if ($entity instanceof EntityInterface) {

      // Return the ID if not given any field names.
      if (empty($this->configuration['field'])) {
        return $entity->id();
      }

      // Get the field if provided.
      $field_name = $this->configuration['field'];
      $field = $entity->get($field_name);

      // Finally, get the value.
      if (!empty($field)) {
        return $field->getValue();
      }
    }

    return NULL;
  }

}
